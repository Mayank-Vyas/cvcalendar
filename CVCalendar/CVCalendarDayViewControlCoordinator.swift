//
//  CVCalendarDayViewControlCoordinator.swift
//  CVCalendar
//
//  Created by E. Mozharovsky on 12/27/14.
//  Copyright (c) 2014 GameApp. All rights reserved.
//

import UIKit

public final class CVCalendarDayViewControlCoordinator {
    // MARK: - Non public properties
    fileprivate var selectionSet = Set<DayView>()
    fileprivate unowned let calendarView: CalendarView
    
    // MARK: - Public properties
    public weak var selectedDayView: CVCalendarDayView?
    
    public var daySelected  = true //for recognizing day and week view
    public var animator: CVCalendarViewAnimator! {
        get {
            return calendarView.animator
        }
    }
    // MARK: - initialization
    public init(calendarView: CalendarView) {
        self.calendarView = calendarView
    }
}

// MARK: - Animator side callback

extension CVCalendarDayViewControlCoordinator {
    public func selectionPerformedOnDayView(_ dayView: DayView) {
        // TODO:
    }
    
    //Select super view of selected day view
    public func selectWeekView(withDayView: CVCalendarDayView) {
        if withDayView.superview != nil && self.calendarView.calendarMode != CalendarMode.weekView && !daySelected {
            //Select week view if user is on week view with transparent color background
            let weekView = withDayView.superview as! CVCalendarWeekView
            for dayView in weekView.dayViews {
                dayView.backgroundColor = IntradiemColor.transparentColor()
            }
        }
    }

    public func deselectionPerformedOnDayView(_ dayView: DayView) {
        if dayView != selectedDayView {
            selectionSet.remove(dayView)
            dayView.setDeselectedWithClearing(true)
        }
 }

    public func dequeueDayView(_ dayView: DayView) {
        selectionSet.remove(dayView)
    }

    
    //Deselect previous selected week view
     public  func deSelectWeekView(withDayView : CVCalendarDayView) {
        if withDayView.superview != nil && self.calendarView.calendarMode != CalendarMode.weekView  {
            let weekView = withDayView.superview as! CVCalendarWeekView
            for dayView in weekView.dayViews {
                dayView.backgroundColor = UIColor.white
            }
        }
}
    
    public func flush() {
        selectedDayView = nil
        selectionSet.removeAll()
    }
}

// MARK: - Animator reference

private extension CVCalendarDayViewControlCoordinator {
    func presentSelectionOnDayView(_ dayView: DayView) {
        animator.animateSelectionOnDayView(dayView)
        //Select week when user tap on any day of month or scroll left right on week view. Selectoin of week in handle inside "selectWeekView" method
        selectWeekView(withDayView: dayView)
    }

    func presentDeselectionOnDayView(_ dayView: DayView) {
        animator.animateDeselectionOnDayView(dayView)
        //Deselect previous week when user tap on any day of month or scroll left right on week view
        deSelectWeekView(withDayView: dayView)
    }

}

// MARK: - Coordinator's control actions

extension CVCalendarDayViewControlCoordinator {
    public func performDayViewSingleSelection(_ dayView: DayView) {
        selectionSet.insert(dayView)

        if selectionSet.count > 1 {
//            let count = selectionSet.count-1
            for dayViewInQueue in selectionSet {
                if dayView != dayViewInQueue {
                    if dayView.calendarView != nil {
                        presentDeselectionOnDayView(dayViewInQueue)
                    }
                }

            }
        }

        if let _ = animator {
            if selectedDayView != dayView {
                selectedDayView = dayView
                presentSelectionOnDayView(dayView)
            }
        }
          
}

    public func performDayViewRangeSelection(_ dayView: DayView) {
        print("Day view range selection found")
    }
}
